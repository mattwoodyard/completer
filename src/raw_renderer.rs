

use types::*;
use std::fmt;
use std::marker::PhantomData;

pub struct RawRenderer<A> (PhantomData<A>);

impl<'a, A: 'a + fmt::Display> RawRenderer<A> {

    pub fn new() -> RawRenderer<A> {
        RawRenderer (
            PhantomData
        )
    }

    fn draw_header(&mut self, app: &CompleteApp<A>) {
        println!("{}", app.filter_state.value.as_str());
    }

    fn draw_line(&mut self, _: usize, item: &A) {

        println!("{}", format!("{}", item));
    }

}



impl<'a, A: 'a + fmt::Display> RenderEngine<'a, CompleteApp<A>, A> for RawRenderer<A> {
    fn start_cycle(&mut self) {}
    fn end_cycle(&mut self) {}
    fn render(&mut self, frame: &CompleteApp<A>) {

        self.start_cycle();
        self.draw_header(&frame);

        println!("{}", frame.paging_state.cached_page.len());
        frame.into_iter().enumerate().map(|(i, item)| {
            self.draw_line(i, item);
        }).count();

        self.end_cycle();
    }


    fn put_text(&mut self, _: usize, _: usize, v: &str, _: Style) {
        println!("{}", v);
    }

}
