extern crate screens;
extern crate rustbox;
extern crate argparse;
extern crate strfmt;

use strfmt::strfmt;
use std::path::PathBuf;
use screens::types::*;
use screens::file_source::LineRecordSource;
use argparse::{ArgumentParser, Store, Collect};
use screens::terminal_event_loop::{event_loop, CompletedResult};
use std::fmt;

use screens::indexing::{NullIndexer, DecIndexer};
use std::collections::HashMap;

use std::process::Command;
use std::os::unix::process::CommandExt;

#[derive(Clone)]
struct Line(String);

impl ParseLine for Line {
    fn parse_line(l: String) -> Option<Line> {
        Some(Line(String::from(l.trim())))
    }
}

impl Identified for Line {

    fn id(&self) -> &str { self.0.as_str() }
    fn search(&self) -> &str { self.0.as_str() }
    fn as_dict(&self) -> HashMap<String, String> {
        let mut hm = HashMap::new();
        hm.insert(String::from("0"), self.0.clone());
        hm
    }

}

impl PartialMatch for Line {
    fn matches(&self, other: &str) -> bool {
        self.0.as_str().to_lowercase().find(&other.to_lowercase()).is_some()
    }
}

impl fmt::Display for Line {
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(fmtr, "{}", self.0)
    }
}


#[derive(Clone)]
struct KVLine(String, String);

impl ParseLine for KVLine {
    fn parse_line(l: String) -> Option<KVLine> {
        let mut split_item = l.trim().split(" ");
        let key = split_item.next();
        let values:Vec<&str> = split_item.collect();

        //TODO move map
        key.map(|k| {
            let v:String = String::from(values.join(" "));
            let v = if v.len() == 0 { String::from(k) } else { v };
            KVLine(String::from(k), v)
        })
    }
}

impl Identified for KVLine {
    fn id(&self) -> &str { self.0.as_str() }
    fn search(&self) -> &str { self.1.as_str() }
    fn as_dict(&self) -> HashMap<String, String> {
        let mut hm = HashMap::new();
        hm.insert(String::from("0"), self.0.clone());
        hm.insert(String::from("1"), self.1.clone());
        hm
    }
}

impl PartialMatch for KVLine {
    fn matches(&self, other: &str) -> bool {
        self.0.as_str().to_lowercase().find(&other.to_lowercase()).is_some() ||
        self.1.as_str().to_lowercase().find(&other.to_lowercase()).is_some()
    }
}

impl fmt::Display for KVLine {
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(fmtr, "{}", self.1)
    }
}



fn main() {

    let mut file_path = String::from("~/.complete");
    let mut execute: Vec<String> = vec![];
    let mut file_parser_type = String::from("line");
    let mut mode = String::from("select");


    {  // this block limits scope of borrows by ap.refer() method
        let mut ap = ArgumentParser::new();
        ap.set_description("Auto complete some stuff.");

        ap.refer(&mut execute)
            .add_argument("execute", Collect,
                          "Execute a command after successful selection");
        ap.refer(&mut file_path)
            .add_option(&["-l", "--list"], Store,
                        "List of completable terms");

        ap.refer(&mut mode)
            .add_option(&["--mode"], Store, "name of start mode");

        ap.refer(&mut file_parser_type)
            .add_option(&["--parser"], Store, "name of parser mode");

        ap.parse_args_or_exit();
    }

    // TODO support stdin
    // TODO switch between them
    // TODO factory for line source

    let (start_mode, start_indexer) =
        match mode.as_str() {
            "select" => { (Mode::Select, Box::new(DecIndexer) as Box<DisplayIndexer>) }
            "filter" => { (Mode::Filter, Box::new(NullIndexer) as Box<DisplayIndexer>) }
            _ => { (Mode::Select, Box::new(DecIndexer) as Box<DisplayIndexer>) }
        };

    let mut fsource: Box<LineRecordSource<KVLine>> = Box::new(screens::file_source::LineRecordSource::new(PathBuf::from(file_path)));
    fsource.init();

    let app = CompleteApp {
        paging_state: Paging::empty(0),
        selection: screens::types::Selection::None,
        filter_state: FilterState::empty(),
        source: fsource,
        prompt: String::from(""),
        mode: start_mode,
        indexer: start_indexer
    };


    match event_loop(app) {
        CompletedResult::Free(s) =>  {
            let mut hm = HashMap::new();
            hm.insert(String::from("0"), s);
                let exec_str:Vec<String> = execute.iter().map(|i:&String| strfmt(i, &hm).unwrap()).collect();
            let mut c = Command::new(exec_str[0].clone());
            for i in {1..exec_str.len()} {
                c.arg(exec_str[i].clone());
            }
            c.exec();
        }
        CompletedResult::Selected(item) => {
            let hm = item.as_dict();
            let exec_str:Vec<String> = execute.iter().map(|i:&String| strfmt(i, &hm).unwrap()).collect();
            let mut c = Command::new(exec_str[0].clone());
            for i in {1..exec_str.len()} {
                c.arg(exec_str[i].clone());
            }
            c.exec();
        }
        CompletedResult::Cancel => {
        }
    };


    }
