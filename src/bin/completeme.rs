// #![feature(core)]

extern crate rustbox;
extern crate argparse;
extern crate core;
extern crate strfmt;
extern crate screens;
use strfmt::strfmt;

use std::process::Command;
use std::os::unix::process::CommandExt;

use std::error::Error;
use std::default::Default;

use rustbox::{Color, RustBox};
use rustbox::Key;

use argparse::{ArgumentParser, StoreTrue, Store, Collect};
use core::str::FromStr;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;

use std::collections::HashMap;
// use screens;
//use screens::view_message::*;
use screens::screens::event_loop::{Task, Screen};


struct Styling {
    fg: rustbox::Color,
    bg: rustbox::Color,
    style: rustbox::Style
}

impl Styling {
    fn new(fg: rustbox::Color, bg: rustbox::Color, style: rustbox::Style) -> Styling {
    Styling { fg: fg, bg: bg, style: style }
}
}

struct ScreenColors {
    list_text: Styling,
    input_box: Styling,
    highlight: Styling
}


impl ScreenColors {
    fn default() -> ScreenColors {
        ScreenColors {
            list_text: Styling::new(Color::White, Color::Default, rustbox::RB_NORMAL),
            input_box: Styling::new(Color::White, Color::Default, rustbox::RB_BOLD),
            highlight: Styling::new(Color::White, Color::Blue, rustbox::RB_BOLD)
        }
    }
}


trait ScreenSource {
    fn refresh(&mut self);
    fn get_range(&mut self, page_start: usize, page_end: usize) -> Vec<Vec<String>>;
    fn len(&self) -> usize;
    fn get_display_line(&self) -> String;
    // TODO Paging 
}

enum LineIdentity {
    FullLine,
    Keyed(usize),
    KeyValued(usize, usize)
}

struct FilteredFileLines {
    file_path: String,
    filter: Vec<String>,
    cached_lines: Vec<Vec<String>>,
    identity: LineIdentity
}

impl FilteredFileLines {
    fn from_file(path: String, identify_by: LineIdentity) -> FilteredFileLines {
        FilteredFileLines {
            file_path: path,
            filter: vec![],
            cached_lines: vec![],
            identity: identify_by
        }
    }

    fn set_filter(&mut self, f: Vec<String>) {
        self.filter = f;
    }

    fn push(&mut self, c: char) {
        if c == ' ' {
            self.filter.push(String::from(""));
        } else {
            if self.filter.len() == 0 {
                self.filter.push(String::from(""));
            } 
            let last = self.filter.len() - 1;
            self.filter[last].push(c);
        }
    }

    fn pop(&mut self) {
        if self.filter.len() == 0 {
        } else {
            let last = self.filter.len() - 1;
            if self.filter[last].len() > 0 {
                let l = self.filter[last].len() - 1;
                self.filter[last].remove(l);
            } else {
                self.filter.remove(last);
            }
        }
    }

}

impl ScreenSource for FilteredFileLines {
    fn get_display_line(&self) -> String {
        self.filter.join(" ")
    }
    fn refresh(&mut self) {
        let path = Path::new(&self.file_path);
        let display = path.display();

        // Open the path in read-only mode, returns `io::Result<File>`
        let file = match File::open(&path) {
            // The `description` method of `io::Error` returns a string that describes the error
            Err(why) => panic!("couldn't open {}: {}", display, Error::description(&why)),
            Ok(file) => file,
        };

        let reader = BufReader::new(file);
        let ucased_filter: Vec<String> = self.filter.iter().map(|i| i.to_uppercase()).collect();

        self.cached_lines =
            reader.lines()
                .map(|i| i.unwrap()
                    .split_whitespace()
                    .map(|j| String::from(j)).collect())
                .collect();

        // Do the filtering inline

        // TOOD should apply if vector != empty
        // fn filter(&self, lines: &Vec<Vec<String>>) -> Vec<Vec<String>> {
        // let upper_buffer: Vec<String> = self.0.iter().map (|j| j.to_uppercase()).collect();
        // lines.iter().filter(|i| upper_buffer.iter().all(|j| i[0].contains(j))).cloned().collect()

        self.cached_lines = self.cached_lines.iter().filter(|i| ucased_filter.iter().all(|j| i[0].contains(j))).cloned().collect();


    }

    // TODO - slices instead
    fn get_range(&mut self, page_start: usize, page_end: usize) -> Vec<Vec<String>> {
        let mut res = vec![];
        for i in page_start..page_end {
            if (i as usize) < self.cached_lines.len() {
                res.push(self.cached_lines[i as usize].clone());
            }
        }
        res
    }

    fn len(&self) -> usize {
        self.cached_lines.len()
    }
}

enum PageMotion {
    PageUp,
    PageDown,
    FocusLine(usize),
    SelectionUp,
    SelectionDown
}



#[derive(Clone,Debug)]
enum Selection {
    None,
    // TODO - IncompleteSingle
    Single(usize),
    Range(usize, usize),
    Multiple(Vec<usize>),
    All
}


impl Selection {
    fn up<S:ScreenSource>(&self, m: &Model<S>) -> Selection {
        match self {
            &Selection::None => { Selection::Single(0) }
            &Selection::Single(idx) => {
                if idx - 1 > 0 {
                    Selection::Single(idx -1)
                } else {
                    Selection::Single(0)
                }
            }
            _ => { Selection::None }
        }
    }
    fn down<S:ScreenSource>(&self, m: &Model<S>) -> Selection {
        match self {
            &Selection::None => { Selection::Single(0) }
            &Selection::Single(idx) => {
                if idx + 1 < m.pages.current_page_content.len() {
                    Selection::Single(idx + 1)
                } else {
                    Selection::Single(idx)
                }
            }
            _ => { Selection::None }
        }

    }
}


impl PartialEq for Selection {

    fn eq(&self, b: &Selection) -> bool {
        match (self, b) {
            (&Selection::Single(i), &Selection::Single(j)) => {
                i == j
            }
            _ => { false }
        }
    }
}

struct PagingModel {
    page: usize,
    page_size: usize,
    current_page_content: Vec<Vec<String>>
}

impl PagingModel {
    /**
     * Shift view down a full page
     */
    fn page_down(&mut self, source: &mut ScreenSource) -> bool {
        if (((self.page + 1) * self.page_size) as usize) <= source.len() {
            self.page = self.page + 1;
            self.current_page_content = self.page_values(source);
            true
        }  else {
            true
        }
    }

    /**
     * Shift view up a full page
     */
    fn page_up(&mut self, source: &mut ScreenSource) -> bool {
        if self.page > 0 {
            self.page = self.page - 1;
            self.current_page_content = self.page_values(source);
            true
        } else {
            true
        }
    }

    /**
     * Select only the values visible on the screen
     * TODO return &Vec<Vec<String>> instead
     */
    fn page_values(&self, source: &mut ScreenSource) -> Vec<Vec<String>> {
        let page_start = self.page * self.page_size;
        let page_end   = (self.page + 1) * self.page_size;
        source.get_range(page_start, page_end)
    }

}


// TODO Error
struct Model<'a, S: ScreenSource + 'a> {
    selection: Selection,
    pages: PagingModel,
    nav_digits: DigitBase,
    colors: ScreenColors,
    prompt: String,
    screen_manager: &'a mut S
}


impl<'a, S: ScreenSource> Model<'a, S> {

    fn get_display_line(&self) -> String {
        let disp = self.screen_manager.get_display_line();
        if disp.len() > 0 {
            disp
        } else {
            match self.selection {
                Selection::None => { String::from("") }
                Selection::Single(v) => { self.nav_digits.index_num_to_string(v) }
                _ => { String::from("Unimpl") }
            }
        }
    }
}



fn load_initial_terms(path: &str) -> Vec<Vec<String>> {
    let path = Path::new(path);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that describes the error
        Err(why) => panic!("couldn't open {}: {}", display, Error::description(&why)),
        Ok(file) => file,
    };

    let reader = BufReader::new(file);
    reader.lines()
          .map(|i| i.unwrap()
                    .split_whitespace()
                    .map(|j| String::from(j)).collect())
          .collect()
}

fn load_initial_terms_kv(path: &str) -> Vec<Vec<String>> {
    let path = Path::new(path);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that describes the error
        Err(why) => panic!("couldn't open {}: {}", display, Error::description(&why)),
        Ok(file) => file,
    };

    let reader = BufReader::new(file);
    reader.lines()
        .map(|i| {
            let mut j = i.unwrap();
            let mut j2 = j.split_whitespace();
            let fst = j2.next();
            let snd:Vec<String> = j2.map(|j| String::from(j)).collect();
            match fst {
                Some(f) => { vec![snd.join(" "), String::from(f)] }
                None => { vec![] }
            }
        }).collect()
}


struct DigitBase(String);


impl DigitBase {

    fn base(&self) -> usize {
        self.0.len()
    }

    fn unshift(&self, b: usize) -> usize {
        b / self.0.len()
    }

    fn shift(&self, b: usize) -> usize {
        b * self.0.len()
    }

    fn digit_to_int(&self, digit: char) -> Option<usize> {
        self.0.find(digit)
    }

    fn int_to_digit(&self, ud: usize) -> Option<char> {
        if ud < self.0.len() {
            self.0.chars().nth(ud)
        } else {
            None
        }
    }

    fn index_num_to_string(&self, number: usize) -> String {
        let mut cv = number;
        let mut rv = String::from("");
        let mut postfix = String::from("");
        if number < self.0.len() {
            match self.int_to_digit(0) {
                None => {}
                Some(v) => {
                    postfix = v.to_string();
                }
            }
        }

        loop {
            if cv >= self.0.len() {
                let cd = cv % self.0.len();
                match self.int_to_digit(cd) {
                    None => { return String::from("XX"); }
                    Some(nd) => {
                        rv = nd.to_string() + rv.as_str();
                    }
                }
                cv = cv / self.0.len();
            } else {
                match self.int_to_digit(cv) {
                    None => { return String::from("XX"); }
                    Some(nd) => {
                        return postfix + nd.to_string().as_str() + rv.as_str();
                    }
                }
            }
        }
    }
}


fn pop_digit<S: ScreenSource>(m: &mut Model<S>) {
    match m.selection {
        Selection::Single(a) => {
            let r = m.nav_digits.unshift(a);
            m.selection = Selection::Single(r);
        }
        _ => {}
    }
}

#[test]
fn test_avy_digt() {
    //let mut db = DigitBase(0);
}

fn handle_avy_digit<S: ScreenSource>(m: &mut Model<S>, v: usize) {
    match m.selection {
        Selection::None => {
                m.selection = Selection::Single(v);
        }
        Selection::Single(a) => {
            m.selection = Selection::Single(m.nav_digits.shift(a) + v);
        }
        _ => { }
    }
}


#[derive(Debug)]
enum Action {
    Launch(Selection),
}

fn handle_key_filter(m: &mut Model<FilteredFileLines>, k: rustbox::Key) -> Task<Action>{
    match k {
        Key::Char(c) => {
            m.screen_manager.push(c);
            m.screen_manager.refresh();
            m.pages.current_page_content = m.pages.page_values(m.screen_manager);
            Task::LoopRender
        }
        Key::Backspace | Key::Delete => {
            m.screen_manager.pop();
            m.screen_manager.refresh();
            m.pages.current_page_content = m.pages.page_values(m.screen_manager);
            Task::LoopRender
        }

        Key::Tab => { Task::Execute(Action::Launch(m.selection.clone())) }
        Key::Enter => {
            if m.pages.current_page_content.len() == 1 {
                m.selection = Selection::Single(0);
            }
            Task::Execute(Action::Launch(m.selection.clone())) }
        Key::Esc => { Task::Exit }
        Key::Right | Key::Left => { Task::Loop }
        Key::Up => {
            m.selection = m.selection.up(m);
            Task::LoopRender
        }
        Key::Down => {
            m.selection = m.selection.down(m);
            Task::LoopRender
        }

        Key::PageUp => {
            //handle_page_motion(PageMotion::GotoLine(-1), m);
            m.pages.page_up(m.screen_manager);
            Task::LoopRender
        }
        Key::PageDown => {
            //handle_page_motion(PageMotion::GotoLine(-1), m);
            m.pages.page_down(m.screen_manager);
            Task::LoopRender
        }
        _ => { Task::Loop }




    }

}


fn handle<S: ScreenSource>(m: &mut Model<S>, k: rustbox::Key) -> Task<Action> {
    match k {
        Key::Char(c) => {
            match m.nav_digits.digit_to_int(c) {
                Some(v) => {
                    handle_avy_digit(m, v);
                    Task::LoopRender
                }
                None => {
                    Task::LoopRender
                }
            }
        }
        Key::Backspace | Key::Delete => {
            pop_digit(m);
            Task::LoopRender
        }

        Key::Tab => { Task::Execute(Action::Launch(m.selection.clone())) }
        Key::Esc => { Task::Exit }
        Key::Enter => { Task::Execute(Action::Launch(m.selection.clone())) }

        Key::Right | Key::Left => { Task::Loop }
        Key::Up => {
           // handle_page_motion(PageMotion::SelectionUp, m);
            Task::LoopRender
        }
        Key::Down => {
            //handle_page_motion(PageMotion::SelectionDown, m);
            Task::LoopRender
        }

        Key::Home => {
            //handle_page_motion(PageMotion::GotoLine(0), m);
            Task::LoopRender
        }
        Key::End => {
        //handle_page_motion(PageMotion::GotoLine(-1), m);
            Task::LoopRender
        }

        Key::PageUp => {
            //handle_page_motion(PageMotion::GotoLine(-1), m);
            m.pages.page_up(m.screen_manager);
            Task::LoopRender
        }
        Key::PageDown => {
            //handle_page_motion(PageMotion::GotoLine(-1), m);
            m.pages.page_down(m.screen_manager);
            Task::LoopRender
        }
        _ => { Task::Loop }

    }
}



fn render_search<S: ScreenSource>(rustbox: &rustbox::RustBox, m: &Model<S>) {
    rustbox.clear();
    rustbox.print(0, 0, m.colors.input_box.style, m.colors.input_box.fg, m.colors.input_box.bg, m.prompt.as_str());

    let cfstr = m.get_display_line();

    rustbox.print(2, 0, m.colors.input_box.style, m.colors.input_box.fg, m.colors.input_box.bg, cfstr.as_str());

    let start_line = 1;
    let indent = 0;
    for i in 0..m.pages.current_page_content.len() {

        match m.selection {
            Selection::Single(idx) => {
                if idx == i {
                    rustbox.print(indent, i + start_line, m.colors.list_text.style, m.colors.list_text.fg, m.colors.list_text.bg, ">");
                    rustbox.print(indent + 1, i + start_line, m.colors.highlight.style, m.colors.highlight.fg, m.colors.highlight.bg, m.pages.current_page_content[i][0].as_str());
                } else {
                    rustbox.print(indent + 1, i + start_line, m.colors.list_text.style, m.colors.list_text.fg, m.colors.list_text.bg, m.pages.current_page_content[i][0].as_str());
                }
            }
            _ => {
                rustbox.print(indent + 1, i + start_line, m.colors.list_text.style, m.colors.list_text.fg, m.colors.list_text.bg, m.pages.current_page_content[i][0].as_str());
            }
        }
    }
    rustbox.print(0, rustbox.height() - 1 , m.colors.input_box.style, m.colors.input_box.fg, m.colors.input_box.bg, m.pages.current_page_content.len().to_string().as_str());
    rustbox.present();
}

fn render<S: ScreenSource>(rustbox: &rustbox::RustBox, m: &Model<S>) {
    rustbox.clear();
    rustbox.print(0, 0, m.colors.input_box.style, m.colors.input_box.fg, m.colors.input_box.bg, m.prompt.as_str());

    let cfstr = match m.selection {
        Selection::None => { String::from("") }
        Selection::Single(v) => { m.nav_digits.index_num_to_string(v) }
        _ => { String::from("Unimpl") }
    };

    rustbox.print(2, 0, m.colors.input_box.style, m.colors.input_box.fg, m.colors.input_box.bg, cfstr.as_str());

    let start_line = 1;
    let indent = 0;
    for i in 0..m.pages.current_page_content.len() {
        let mut o = m.nav_digits.index_num_to_string(i);

        if cfstr.len() > 0 && o.as_str().starts_with(cfstr.as_str()) {
            rustbox.print(indent, i + start_line, m.colors.highlight.style, m.colors.highlight.fg, m.colors.highlight.bg, o.as_str());
        } else {
            rustbox.print(indent, i + start_line, m.colors.list_text.style, m.colors.list_text.fg, m.colors.list_text.bg, o.as_str());
        }

        // TODO highlight longest prefix match

        if Selection::Single(i) == m.selection {
        // if Selection::Single(i ) == m.selection {
            rustbox.print(indent + 3, i + start_line, m.colors.highlight.style, m.colors.highlight.fg, m.colors.highlight.bg, m.pages.current_page_content[i][0].as_str());
        } else {
            rustbox.print(indent + 3, i + start_line, m.colors.list_text.style, m.colors.list_text.fg, m.colors.list_text.bg, m.pages.current_page_content[i][0].as_str());
        }
    }

    rustbox.print(0, rustbox.height() - 1 , m.colors.input_box.style, m.colors.input_box.fg, m.colors.input_box.bg, m.pages.current_page_content.len().to_string().as_str());
    rustbox.present();
}


fn execute<S: ScreenSource>(rb: &rustbox::RustBox, m: &Model<S>, t: &Action) -> Task<Action> {
    match t {
        &Action::Launch(ref s) => {
            match s {
                // TODO rebase the base so that theres no funny business here
                &Selection::Single(t) => {
                    Task::RunExternal(m.pages.current_page_content[t].clone())
                }
                _ => { Task::Loop }
            }
        }
    }
}

fn event_loop(source: &mut FilteredFileLines) -> Option<Vec<String>> {

    let rustbox = match RustBox::init(Default::default()) {
        Result::Ok(v) => v,
        Result::Err(e) => panic!("{}", e),
    };


    // page_size: rustbox.height() - 2, // TODO calculate from nav_buffer_ list offset
    let mut model = Model {
        selection: Selection::None,
        pages: PagingModel {page: 0, page_size: rustbox.height() - 2, current_page_content: vec![] },
        nav_digits: DigitBase(String::from("asdfghjkl")),
        colors: ScreenColors::default(),
        prompt: String::from(">"),
        screen_manager: source
    };

    model.screen_manager.refresh();
    model.pages.current_page_content = model.pages.page_values(model.screen_manager);
    render(&rustbox, &model);

    loop {
        match rustbox.poll_event(false) {
            Ok(rustbox::Event::KeyEvent(key)) => {
                //match handle_key_filter(&mut model, key) {
                match handle(&mut model, key) {
                    Task::Exit => { break; }
                    Task::Loop => {}
                    Task::LoopRender => { render(&rustbox, &model); }
                    Task::RunExternal(e) => { return Some(e); }
                    Task::Execute(a) => {
                        match execute(&rustbox, &model, &a) {
                            Task::RunExternal(e) => { return Some(e); }
                            tem@_ => { }
                        }
                    }
                }
            }
            Err(e) => { panic!("{}", e.description()) }
            _ => { }
        }
    }
    None
}

fn main() {

    let mut file_path = String::from("~/.complete");
    let mut execute: Vec<String> = vec![];

    //let mut handlers = HashMap<String, FnMut()>::new();

    // TODO parser

    {  // this block limits scope of borrows by ap.refer() method
        let mut ap = ArgumentParser::new();
        ap.set_description("Auto complete some stuff.");

        ap.refer(&mut execute)
            .add_argument("execute", Collect,
                          "Execute a command after successful selection");
        ap.refer(&mut file_path)
            .add_option(&["-l", "--list"], Store,
                        "List of completable terms");

        ap.parse_args_or_exit();
    }

    let mut filef = FilteredFileLines::from_file(file_path, LineIdentity::FullLine);

    match event_loop(&mut filef) {
        Some(res) => {
            let mut hm = HashMap::new();
            for i in {0..res.len()} {
                // TODO how to move
                hm.insert(i.to_string(), res[i].clone());
            }
            println!("{:?}", hm);

            let exec_str:Vec<String> = execute.iter().map(|i:&String| strfmt(i, &hm).unwrap()).collect();
            let mut c = Command::new(exec_str[0].clone());
            for i in {1..exec_str.len()} {
                c.arg(exec_str[i].clone());
            }

            c.exec();

        }
        None => { }
    }

}
