use rustbox;
use updates::update;
use types::*;
use terminal_renderer::TerminalRenderer;
use std::error::Error;
use types::RenderEngine;
use std::fmt;
use rustbox::Key;
// use raw_renderer::RawRenderer;
use indexing;


fn on_key_select<A>(_: &CompleteApp<A>, k: rustbox::Key) -> UiAction {
    match k {
        Key::Char(c) => {
            UiAction::Edit(EditAction::Insert(c))
        }

        Key::PageDown => { UiAction::View(ViewAction::PageDown(1)) }
        Key::PageUp => { UiAction::View(ViewAction::PageUp(1)) }
        Key::Backspace => { UiAction::Edit(EditAction::Backspace) }
        Key::Enter => { UiAction::Execute }
        Key::Esc => { UiAction::Cancel }
        Key::Ctrl(c) => {
            if c == 'l' {
                UiAction::SwitchMode
            } else {
                UiAction::NoOp
            }
        }
        _ => {
            UiAction::NoOp
        }
    }
}

fn on_key_filter<A>(_: &CompleteApp<A>, k: rustbox::Key) -> UiAction {
    match k {
        Key::Char(c) => {
            UiAction::Edit(EditAction::Insert(c))
        }
        Key::Enter => { UiAction::Execute }
        Key::Esc => { UiAction::Cancel }
        Key::Tab => { UiAction::Selection(SelectionAction::ToFilter) }
        Key::PageDown => { UiAction::View(ViewAction::PageDown(1)) }
        Key::PageUp => { UiAction::View(ViewAction::PageUp(1)) }
        Key::Up => { UiAction::Selection(SelectionAction::Up) }
        Key::Down => { UiAction::Selection(SelectionAction::Down) }
        Key::Delete => { UiAction::Edit(EditAction::DeleteAtCursor) }
        Key::Backspace => { UiAction::Edit(EditAction::Backspace) }
        Key::Ctrl(c) => {
            if c == 'l' {
                UiAction::SwitchMode
            } else {
                UiAction::NoOp
            }
        }
        _ => {
            UiAction::NoOp
        }
    }
}

fn on_key<A>(app: &CompleteApp<A>, k: rustbox::Key) -> UiAction {
    match app.mode {
        Mode::Select => { on_key_select(app, k) }
        Mode::Filter => { on_key_filter(app, k) }
    }
}

pub enum CompletedResult<A> {
    Free(String),
    Selected(A),
    Cancel
}

pub fn event_loop<A: fmt::Display + Clone + Identified>(mut app: CompleteApp<A>)  -> CompletedResult<A> {
    let mut renderer: TerminalRenderer<A> = TerminalRenderer::new();
    app.paging_state.update_page_size(renderer.rustbox.height() - 2);
    app.paging_state.page(app.source.apply_filter(&app.filter_state));
    renderer.render(&app);

    loop {
        match renderer.rustbox.poll_event(false) {
            Ok(rustbox::Event::KeyEvent(key)) => {
                let act = on_key(&app, key);

                if let UiAction::SwitchMode = act {
                    match app.mode {
                        Mode::Filter => {
                            app.mode = Mode::Select;
                            app.indexer = Box::new(indexing::DecIndexer);
                        }
                        Mode::Select => {
                            app.mode = Mode::Filter;
                            app.indexer = Box::new(indexing::NullIndexer);
                        }
                    }
                }

                match update(app, act) {
                    AppState::Done(result) => {
                        drop(renderer);
                        return CompletedResult::Selected(result);
                    }
                    AppState::DoneFree(result) => {
                        drop(renderer);
                        return CompletedResult::Free(result);
                    }
                    AppState::Continue(appn) => {
                        app = appn;
                    }
                }
            }
            Ok(_) => {}
            Err(e) => {
                panic!("Err: {}", e.description());
            }
        }

        renderer.render(&app);
    }
}
