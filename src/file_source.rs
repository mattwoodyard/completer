

use types::{CompleteSource, FilterState, PartialMatch, ParseLine};
use std::path::PathBuf;
use std::fs::File;
use std::io::{BufReader,BufRead};


//TODO error handling
pub struct LineRecordSource<A: PartialMatch> {
    file_name: PathBuf,
    all_lines: Vec<A>,
    cache: Vec<A>
    // visible: Vec<&'a A>
}

impl<A: PartialMatch + ParseLine> LineRecordSource<A> {
    pub fn new(fp: PathBuf) -> LineRecordSource<A> {
        LineRecordSource {
            file_name: fp,
            all_lines: vec![],
            cache: vec![]
        }
    }

    fn load_file(&mut self) {
        let file = File::open(self.file_name.clone()).unwrap();
        let mut br = BufReader::new(file);

        loop {
            let mut buf:String = String::new();
            match br.read_line(&mut buf) {
                Err(e) => {
                    println!("{:?}", e);
                    break;
                }
                Ok(read) => {
                    if read == 0 {
                        break;
                    }
                    match A::parse_line(buf) {
                        Some(item) => {
                            self.all_lines.push(item);
                        }
                        None => {}
                    }
                }
            }
        }
    }
}


impl<A: PartialMatch + Clone + ParseLine> CompleteSource for LineRecordSource<A> {
    type Item = A;

    fn init(&mut self) {
        self.load_file();
        self.cache = self.all_lines.iter().cloned().collect();
    }

    fn apply_filter(&mut self, f: &FilterState) -> &Vec<Self::Item> {
        self.cache = self.all_lines.iter().filter(|item| {
            item.matches(f.value.as_str())
        }).cloned().collect();
        &self.cache
    }

    fn get(&self, idx: usize) -> Option<Self::Item> {
        self.cache.get(idx).cloned()
    }

    fn visible_count(&self) -> usize {
        self.cache.len()
    }
}
