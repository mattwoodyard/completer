use types::*;
use std::fmt;
use rustbox;
use std::marker::PhantomData;
use rustbox::{Color, RustBox};

pub struct Styling {
    fg: rustbox::Color,
    bg: rustbox::Color,
    style: rustbox::Style
}

impl Styling {
    fn new(fg: rustbox::Color, bg: rustbox::Color, style: rustbox::Style) -> Styling {
        Styling { fg: fg, bg: bg, style: style }
    }
}

pub struct ScreenColors {
    pub normal: Styling,
    pub mute: Styling,
    pub inverse: Styling,
    pub highlight: Styling
}


impl ScreenColors {
    fn default() -> ScreenColors {
        ScreenColors {
            normal: Styling::new(Color::White, Color::Default, rustbox::RB_NORMAL),
            mute: Styling::new(Color::White, Color::Default, rustbox::RB_NORMAL),
            inverse: Styling::new(Color::White, Color::Default, rustbox::RB_BOLD),
            highlight: Styling::new(Color::White, Color::Blue, rustbox::RB_BOLD)
        }
    }
}


pub struct TerminalRenderer<'a, A>
    where A: 'a
{
    pub rustbox: rustbox::RustBox,
    pub colors: ScreenColors,
    _a: PhantomData<&'a A>
}

impl<'a, A> TerminalRenderer<'a, A> {

    pub fn new() -> TerminalRenderer<'a, A> {
        let rustbox = match rustbox::RustBox::init(Default::default()) {
            Result::Ok(v) => v,
            Result::Err(e) => panic!("{}", e),
        };

        TerminalRenderer {
            rustbox: rustbox,
            colors: ScreenColors::default(),
            _a: PhantomData
        }
    }

}


impl<'a, A: 'a + fmt::Display> TerminalRenderer<'a, A> {

    fn draw_header(&mut self, app: &CompleteApp<A>) {
        self.put_text( 0, 0 , ">", Style::Normal );
        self.put_text( 4, 0 , app.filter_state.value.as_str(), Style::Normal );
    }

    fn draw_line(&mut self, indexer: &DisplayIndexer, idx: usize, item: &A, style: Style) {
        // TODO fixed width handling
        self.put_text( 1, idx + 2 , format!("{} {}", indexer.as_display(idx), item).as_str(),  style);
    }
}


impl<'a, A: fmt::Display + 'a> RenderEngine<'a, CompleteApp<A>, A> for TerminalRenderer<'a, A>
{
    fn start_cycle(&mut self) {
        self.rustbox.clear();
    }

    fn end_cycle(&mut self) {
        self.rustbox.present();
    }

    fn render(&mut self, frame: &CompleteApp<A>) {

        self.start_cycle();
        self.draw_header(&frame);

        // This is busted
        frame.into_iter().enumerate().map(|(i, item)| {
            let style = if frame.selection.selected(i) {
                Style::Highlight
            } else {
                Style::Normal
            };

            self.draw_line(frame.indexer.as_ref(), i, item, style);
        }).count();

        self.end_cycle();
    }

    fn put_text(&mut self, x: usize, y: usize, v: &str, s: Style) {
        match s {
            Style::Normal => {
                self.rustbox.print(x, y, self.colors.normal.style, self.colors.normal.fg, self.colors.normal.bg, v);
            }
            Style::Highlight => {
                self.rustbox.print(x, y, self.colors.highlight.style, self.colors.highlight.fg, self.colors.highlight.bg, v);
            }
            Style::Inverse => {
                self.rustbox.print(x, y, self.colors.inverse.style, self.colors.inverse.fg, self.colors.inverse.bg, v);
            }
            Style::Mute => {
                self.rustbox.print(x, y, self.colors.mute.style, self.colors.mute.fg, self.colors.mute.bg, v);
            }
        }

    }

}


// struct NoIndexItemRenderer;

// impl<'a, A: 'a> ItemRenderer<'a> for NoIndexItemRenderer
//     where A: fmt::Display + 'a
// {
//     type A = A;
//     type B = CompleteApp<Self::A>;


//     fn draw_line<F>(&mut self, index: usize, item: &'a A, putF: F)
//         where F: FnOnce(usize, usize, &str) -> () {

//     }

//     fn draw_header<F>(&mut self, obj: &CompleteApp<A>, putF: F)
//         where F: FnOnce(usize, usize, &str) -> () {
//         putF(10, 0, obj.filter_state.value.as_str());
//     }


// }

