



use types::*;


fn backprop_filter<A: Clone + Identified>(app: &mut CompleteApp<A>) {
    let fstring = app.selection
        .get(&app.paging_state)
        .map(|item| item.search());
    match fstring {
        None => {}
        Some(fstring) => {
            app.filter_state.set_filter(fstring);
            app.selection = Selection::Single(0);
        }
    }
}

fn update_selection<A: Clone + Identified>(app: &mut CompleteApp<A>, action: SelectionAction) {
    match action {
        SelectionAction::Up => { app.selection.up(app.paging_state.page_size); }
        SelectionAction::Down => { app.selection.down(app.paging_state.page_size); }

        SelectionAction::Set(row) => {
            app.selection = Selection::Single(row);
        }
        // TODO so this is callable refactor
        SelectionAction::ToFilter => {
            backprop_filter(app);
            refresh_page(app);
        }
    }
}

fn refresh_page<A: Clone + Identified>(app: &mut CompleteApp<A>) {
    app.paging_state.page = 0;
    app.paging_state.page(app.source.apply_filter(&app.filter_state));
}

fn update_edit<A: Clone + Identified>(app: &mut CompleteApp<A>, action: EditAction) {
    app.selection = Selection::None;
    match action {
        EditAction::Insert(c) => {
            app.filter_state.insert(c);
        }
        EditAction::DeleteAtCursor => {
            app.filter_state.delete();
        }
        EditAction::Backspace => {
            app.filter_state.backspace();
        }
        EditAction::CursorTo(sz) => {
            app.filter_state.set_cursor(sz);
        }
    }

    match app.mode {
        Mode::Select => {
            // TODO create a selection
            app.selection =
                app.indexer
                .from(app.filter_state.value.clone())
                .map(|r| app.indexer.to_selection(r))
                .unwrap_or(Selection::None);
        }
        Mode::Filter => {
            refresh_page(app);
            // When we've increased the filter
            // 
            match action {
                EditAction::Insert(_) => {
                    if app.paging_state.cached_page.len() == 1 {
                        update_selection(app, SelectionAction::Set(0));
                        backprop_filter(app);
                    }
                }
                _ => {}
            }
        }
    }
}

fn update_view<A: Clone>(app: &mut CompleteApp<A>, action: ViewAction) {
    match action {
        ViewAction::PageDown(_) => {
            app.paging_state.scroll_down(1, app.source.visible_count());
            app.paging_state.page(app.source.apply_filter(&app.filter_state));
        }
        ViewAction::PageUp(_) =>{
            app.paging_state.scroll_up(1, app.source.visible_count());
            app.paging_state.page(app.source.apply_filter(&app.filter_state));
        }
    }
}

pub fn update<A: Clone + Identified>(mut app: CompleteApp<A>, action: UiAction ) -> AppState<A> {
    match action {
        UiAction::NoOp => { AppState::Continue(app) }
        UiAction::SwitchMode => { AppState::Continue(app) }
        UiAction::Selection(action) => {
            update_selection(&mut app, action);
            AppState::Continue(app)
        }
        UiAction::Edit(action) => {
            update_edit(&mut app, action);
            AppState::Continue(app)
        }
        UiAction::View(action) => {
            update_view(&mut app, action);
            AppState::Continue(app)
        }
        UiAction::Cancel => {
            AppState::DoneFree(String::from(""))
        }
        UiAction::Execute => {
            // TODO must select or not
            app.get_selected()
                .map(|s| AppState::Done(s.clone()))
                .unwrap_or(AppState::DoneFree(app.filter_state.value.clone()))
        }
    }
}
