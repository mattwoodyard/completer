


use core::convert::From;

use types::{Index,Selection,DisplayIndexer};



pub struct NullIndexer;

impl DisplayIndexer for NullIndexer {

    fn to_selection(&self, _: Index) -> Selection {
        Selection::None
    }

    fn from(&self, _: String) -> Result<Index, String> {
        Ok(Index { display: String::new(), scaled: 0 })
    }

    fn as_display(&self, _: usize) -> String {
        String::new()
    }

}

pub struct AvyIndexer(String);

impl AvyIndexer {

    fn digit_value(&self, d: char) -> Option<usize> {
        self.0.find(d)
    }

    fn digit(&self, i: usize) -> Option<char> {
        // TODO direct index
        self.0.chars().nth(i)
    }


}

impl DisplayIndexer for AvyIndexer {

    fn to_selection(&self, i: Index) -> Selection {
        let base = self.0.len();
        if i.scaled < base  {
            Selection::Range(i.scaled * base - base, (i.scaled + 1) * base - (base + 1))
        } else {
            Selection::Single(i.scaled - base)
        }
    }
    fn from(&self, s: String) -> Result<Index, String> {
        let mxpos = s.len();
        let mut cnt = 0;

        for (pos, v) in s.chars().enumerate() {
            let power = mxpos - pos;
            // Sort of unsaffe
            let x = self.0.len().pow(power as u32);
            match self.digit_value(v) {
                None => { return Err(String::from("Invalid digit")); }
                Some(i) => {
                    cnt = cnt + x*i;
                }
            }
        }
//        cnt = cnt - self.0.len();
        Ok(Index{display: s.clone(), scaled: cnt })
    }

    fn as_display(&self, i: usize) -> String {
        let base = self.0.len();
        let mut result = String::new();
        let mut i = i + base;
        loop {
            if i == 0 {
                result = result.chars().rev().collect();
                return result;
            }
            let nxt = i % base;
            result.push(self.digit(nxt).unwrap());
            i = i - nxt;
            i = i / base;
        }
    }

}


pub struct DecIndexer;

impl DisplayIndexer for DecIndexer {
    fn to_selection(&self, i: Index) -> Selection {
        let base = 10;
        if i.scaled < base  {
            Selection::Range(i.scaled * base - base, (i.scaled + 1) * base - (base + 1))
        } else {
            Selection::Single(i.scaled - base)
        }
    }


    fn from(&self, s: String) -> Result<Index, String> {
        s.parse::<usize>()
            .map(|i| Index { display: String::new(), scaled: i })
            .map_err(|e| format!("{}", e))
    }

    fn as_display(&self, i: usize) -> String {
        format!("{}", i + 10)
    }
}

