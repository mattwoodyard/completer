extern crate rustbox;


// trait Handler<A> {
//     fn handle(&self, model: &A, rustbox::Key) -> Task<B>;
// }


#[derive(Debug)]
pub enum Task<A> {
    Loop,
    LoopRender,
    Execute(A),
    //Error(String, bool)
    Exit,
    // NavBack 
    RunExternal(Vec<String>)
}

pub trait Screen<A: ScreenSource, B> {
    fn render(a: &A, rb: &rustbox::RustBox);
    fn handle(model: &mut A, k: rustbox::Key) -> Task<B>;
    fn run(a: &mut A, r: &rustbox::RustBox) -> Task<B>;
}

pub trait ScreenSource {
    fn refresh(&mut self) -> Vec<Vec<String>>;
}







