use std::slice;
use std::collections::HashMap;



// pub enum RowIndex {
//     Page(usize),
//     Filter(usize),
//     Data(usize)
// }


pub struct Paging<A> {
    pub page: usize,
    pub page_size: usize,
    pub cached_page: Vec<A>
}

// TODO remove A: Clone
//             that would require another 'a
impl<A: Clone> Paging<A> {
    /**
    return the start index and end index of into the 
    */
    fn get_index_extents(&self) -> (usize, usize) {
        (self.page * self.page_size, (self.page + 1) * self.page_size)
    }

    pub fn update_page_size(&mut self, sz: usize) {
        // TODO this should convert our current page location into a new page location and scroll to there
        self.page_size = sz;
    }

    pub fn page(&mut self, items: &Vec<A>) {
        let (start, end) = self.get_index_extents();
        self.cached_page = items.iter()
            .skip(start)
            .take(end - start)
            .cloned()
            .collect();
    }

    pub fn scroll_down(&mut self, amount: usize, max: usize) {
        if (self.page * self.page_size) + (amount * self.page_size) < max {
            self.page = self.page + amount;
        }
    }

    //TODO shift by absurdist amounts
    pub fn scroll_up(&mut self, amount: usize, _: usize) {
        if (self.page * self.page_size) >= (amount * self.page_size) {
            self.page = self.page - amount;
        }
    }

    pub fn empty(page_size: usize) -> Paging<A> {
        Paging {
            page: 0,
            page_size: page_size,
            cached_page: vec![]
        }
    }

    pub fn get(&self, idx: usize) -> Option<&A> {
        self.cached_page.get(idx)
    }


}

#[test]
fn extent_test() {
    let mut page1: Paging<u8> = Paging { page: 0, page_size: 100, cached_page: vec![] };
    assert_eq!(page1.get_index_extents(), (0, 100));
    page1.page = 3;
    assert_eq!(page1.get_index_extents(), (300, 400));

    page1.scroll_up(1, 10000);
    assert_eq!(page1.page, 2);
}

pub trait PartialMatch {
    fn matches(&self, criteria: &str) -> bool;
}

pub trait Indexer {
    type IndexType;

    fn index_to_linenum(&self, idx: usize) -> Self::IndexType;
    fn linenum_to_index(&self, idx: Self::IndexType) -> usize;
}

pub trait CompleteSource {
    type Item;

    fn init(&mut self);
    fn apply_filter(&mut self, f: &FilterState) -> &Vec<Self::Item>;
    fn get(&self, idx: usize) -> Option<Self::Item>;
    fn visible_count(&self) -> usize;
}

pub enum Style {
    Normal,
    Highlight,
    Inverse,
    Mute
}

pub trait RenderEngine<'a, B, A: 'a>
{
    fn start_cycle(&mut self);
    fn end_cycle(&mut self);
    // fn item_renderer(&mut self) -> Box<ItemRenderer<'a, A=A, B=B>>;
    fn render(&mut self, frame: &B);
    fn put_text(&mut self, x: usize, y: usize, v: &str, s: Style);

}



pub trait ItemRenderer<'a>
{
    type A;
    type B;
    fn draw_line<F>(&mut self, index: usize, item: &'a Self::A, put_f: F)
        where F: FnOnce(usize, usize, &str) -> () ;

    fn draw_header<F>(&mut self, obj: &Self::B, put_f: F)
        where F: FnOnce(usize, usize, &str) -> () ;
}


pub trait ParseLine where Self: Sized {
    fn parse_line(line: String) -> Option<Self>;
}

pub struct FilterState {
    pub value: String,
    cursor_position: usize
}

impl FilterState {
    pub fn empty() -> FilterState {
        FilterState { value : String::new(), cursor_position: 0 } 
    }

    pub fn set_cursor(&mut self, pos: usize) {
        if pos < self.value.len() {
            self.cursor_position = pos;
        } else {
            self.cursor_position = self.value.len() - 1;
        }
    }

    pub fn backspace(&mut self) {
        if self.cursor_position > 0 {
            let cp = self.cursor_position;
            self.set_cursor(cp - 1);
            self.delete();
        }
    }

    pub fn delete(&mut self) {
        self.value.remove(self.cursor_position);
    }

    pub fn insert(&mut self, ch: char) {
        self.value.insert(self.cursor_position, ch);
        self.cursor_position = self.cursor_position + 1;
    }

    pub fn set_filter(&mut self, s: &str) {
        self.value = String::from(s);
        self.cursor_position = self.value.len();
    }

}

#[test]
fn cursor_test() {

    let mut fst = FilterState { value: String::from(""), cursor_position: 0 };

    fst.insert('c');
    fst.insert('h');
    assert_eq!(fst.value, String::from("ch"));

    fst.set_cursor(1);
    fst.insert('t');
    assert_eq!(fst.value, String::from("cth"));

    fst.backspace();
    assert_eq!(fst.value, String::from("ch"));
    fst.backspace();
    assert_eq!(fst.value, String::from("h"));
    fst.backspace();
    assert_eq!(fst.value, String::from("h"));

}

// Selections are in 'page coordinates'
#[derive(Debug,PartialEq,PartialOrd)]
pub enum Selection {
    None,
    Single(usize),
    Range(usize, usize)
}


fn clamped_minus(a: usize, b: usize) -> usize {
    if a >= b { a - b } else { 0 }
}

fn clamped_plus(a: usize, b: usize, mx: usize) -> usize {
    if a + b < mx { a + b } else { mx - 1 }
}



impl Selection {

    pub fn up(&mut self, _: usize) {
        match self {
            &mut Selection::None => { *self = Selection::Single(0); }
            &mut Selection::Single(i) => {
                *self = Selection::Single(clamped_minus(i, 1));
            }
            &mut Selection::Range(s, t) => {
                *self = Selection::Range(clamped_minus(s, 1), clamped_minus(t, 1));
            }
        }
    }

    pub fn down(&mut self, max_index: usize) {
        match self {
            &mut Selection::None => { *self = Selection::Single(0); }
            &mut Selection::Single(i) => {
                *self = Selection::Single(clamped_plus(i, 1, max_index));
            }
            &mut Selection::Range(s, t) => {
                *self = Selection::Range(
                    clamped_plus(s, 1, max_index),
                    clamped_plus(t, 1, max_index));
            }

        }
    }

    pub fn selected(&self, item_index: usize) -> bool {
        match self {
            &Selection::None => { false }
            &Selection::Single(i) => {
                i == item_index
            }
            &Selection::Range(s, t) => {
                // TODO is this an off by one?
                item_index >= s && item_index <= t
            }
        }
    }

    pub fn index(&self, page_index: usize) -> Option<usize> {
        match self {
            &Selection::None => { None }
            &Selection::Single(i) => {
                Some(page_index + i)
            }
            &Selection::Range(_, _) => {
                None
            }
        }
    }


    pub fn get<'a, A: Clone>(&self, page: &'a Paging<A>) -> Option<&'a A> {
        match self {
            &Selection::None => { None }
            &Selection::Single(i) => {
                page.get(i)
            }
            &Selection::Range(_, _) => {
                None
            }
        }
    }
}


pub enum AppState<A> {
    Continue(CompleteApp<A>),
    DoneFree(String),
    Done(A)
}

pub struct CompleteApp<A> {
    pub paging_state: Paging<A>,
    pub selection: Selection,
    pub filter_state: FilterState,
    pub source: Box<CompleteSource<Item=A>>,
    pub prompt: String,
    pub mode: Mode,
    pub indexer: Box<DisplayIndexer>
}




impl<'a, T: 'a> IntoIterator for &'a CompleteApp<T> {
    type Item = &'a T;
    type IntoIter = slice::Iter<'a, T>;

    fn into_iter(self) -> slice::Iter<'a, T> {
        self.paging_state.cached_page.iter()
    }

}

impl<A: Clone> CompleteApp<A> {

    pub fn get_selected(&self) -> Option<&A> {
        self.selection.get(&self.paging_state)
    }
}


pub enum SelectionAction {
    Up,
    Down,
    /**
    offset from the start of the current page
    */
    Set(usize),
    ToFilter
}

pub enum ViewAction {
    PageUp(usize),
    PageDown(usize),
}

pub enum EditAction {
    Insert(char),
    DeleteAtCursor,
    Backspace,
    CursorTo(usize)
}

#[derive(Clone)]
pub enum Mode {
    Filter,
    Select,
}

pub enum UiAction {
    Selection(SelectionAction),
    View(ViewAction),
    Edit(EditAction),
    Execute,
    SwitchMode,
    Cancel,
    NoOp
}


pub trait UiHandler {
    fn on_key<K>(k: K) -> UiAction;
}

pub trait Identified {
    fn id(&self) -> &str;
    fn search(&self) -> &str;
    fn as_dict(&self) -> HashMap<String, String>;
}

pub trait DisplayIndexer
{
    fn to_selection(&self, i: Index) -> Selection;
    fn from(&self, s: String) -> Result<Index, String>;
    fn as_display(&self, i: usize) -> String;
}

pub struct Index {
    pub display: String,
    pub scaled: usize
}




#[test]
fn test_selection() {
    let mut sel = Selection::None;
    sel.up(100);
    assert_eq!(sel, Selection::Single(0));
    sel.up(100);
    assert_eq!(sel, Selection::Single(0));
    sel.down(100);
    assert_eq!(sel, Selection::Single(1));
    sel = Selection::Single(99);
    sel.down(100);
    assert_eq!(sel, Selection::Single(99));
    sel.up(100);
    assert_eq!(sel, Selection::Single(98));
    sel.down(100);
    assert_eq!(sel, Selection::Single(99));
    sel = Selection::None;
    sel.down(100);
    assert_eq!(sel, Selection::Single(0));
}



