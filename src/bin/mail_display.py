#!/usr/bin/python3
import email.parser
import jinja2
import argparse
import os
import sys

ap = argparse.ArgumentParser()
ap.add_argument("path")

def format_html(path):
    headers = email.parser.Parser().parse(open(path, 'r'))

    for w in headers.walk():
        if w.get_content_maintype() == 'multipart':
            continue
        mbody = w.get_payload()

if __name__ == "__main__":
    ctxt = ap.parse_args(sys.argv[1:])
    format_html(ctxt.path)



